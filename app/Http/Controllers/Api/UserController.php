<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function login(Request $request){
        $values = $request->all();

        $user = User::where('email', $values['email'])->first();

        if (!$user || !Hash::check($values['password'], $user->password)) {
            return response()->json([
                'errors' => [
                    'email' => 'E-mail ou senha incorreta.'
                ]
            ], 422);
        }
        return response()->json([
            'user'=>$user
        ],200);
    }
    public function store(Request $request){

        $values = $request->all();

        $getUser = User::where('email',$values['email'])->first();

        if($getUser){
            return response()->json([
                'message'=>'Já existe um usuário com esse email'
            ],400 );
        }

        $values['password'] = bcrypt($values['password']);

        $user = User::create($values);

        return response()->json([
            'data' => [
                'user' => $user,
                'message'=>"Usuário cadastrado com sucesso"
            ]
            ],200);
    }
}
